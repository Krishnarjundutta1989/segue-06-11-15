//
//  ViewController.m
//  TEst Segue
//
//  Created by click labs 115 on 11/4/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSMutableArray *destdata;
    NSMutableArray * currentdata;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    currentdata  = [NSMutableArray new];
    [currentdata addObject:@"kosal"];
    [currentdata addObject:@"rohit"];

    
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"Go"])
    {
        DestinationViewController*dest = segue.destinationViewController;
        dest.destdata = currentdata;
    }
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
