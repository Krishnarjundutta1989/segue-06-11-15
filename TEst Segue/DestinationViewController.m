//
//  DestinationViewController.m
//  TEst Segue
//
//  Created by click labs 115 on 11/4/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "DestinationViewController.h"

@interface DestinationViewController ()
@property (strong, nonatomic) IBOutlet UILabel *lblResult;

@end

@implementation DestinationViewController
@synthesize destdata;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *str = [destdata objectAtIndex:0];
    NSLog(@"%@",str);
    _lblResult.text = [NSString stringWithFormat:@"%@",str];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
